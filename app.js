const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

let mainWindow = null

// Wait until the app is ready
app.once('ready', () => {
  // Create a new mainWindow
  mainWindow = new BrowserWindow({
    // Set the initial width to 800px
    width: 1200,
    // Set the initial height to 600px
    height: 900,
    // Set the default background color of the mainWindow to match the CSS
    // background color of the page, this prevents any white flickering
      // backgroundColor: "#D6D8DC",
    // Don't show the mainWindow until it's ready, this prevents any white flickering
    show: false
  })

  // Load a URL in the mainWindow to the local index.html path
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // mainWindow.webContents.openDevTools()

  // Show mainWindow when page is ready
  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })
})
